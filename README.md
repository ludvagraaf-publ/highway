# highway

HIGHWAY is a retro game similar to Frogger. Is written in Python.
 It uses [Tk interface package](https://docs.python.org/3/library/tkinter.html#module-tkinter).


## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install tkinter.

```bash
pip install tkinter
```

On older macOS (BigSur) you will need to install it from Brew.
Install [brew](https://brew.sh/) first if you haven't done so already.
Then install tkinter for specific version of Python (eg 3.10):

```bash
brew install python-tk@3.10
```

## Usage

In the directory where you download/clone:

```bash
cd ~/Download/highway/
python ./main.py
```

### Playing the game:

The Turtle crawl through highway full of cars. It can go just up-forward.
 Press "Up" arrow key to control it. For quit press "q" .

Enjoy the game!

