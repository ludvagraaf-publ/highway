from turtle import Screen
from engine import Cars, Frog, Level
import time

# starting game speed
CARS_SPEED = 0.25

screen = Screen()
screen.title("highway turtle")
screen.setup(width=600, height=600)

screen.tracer(0)
screen.listen()

level_counter = Level()
my_frog = Frog()
my_frog.create_frog()

cars = Cars()


screen.onkey(my_frog.move_fwd, "Up")
screen.onkey(screen.bye, "q")

car_counter = 10
cars.create_car(10)
game_on = True
while game_on:
    screen.update()
    time.sleep(CARS_SPEED)
    car_counter += 1
    if car_counter % 2:
        cars.create_car(1)

    cars.move_car()

    # if Frog/Turtle reach the top
    if my_frog.ycor() > 280:
        level_counter.increase_level()
        my_frog.reset_frog()
        # speedup cars
        CARS_SPEED *= 0.9
    #collision with the car
    for car in cars.all_cars:
        if car.distance(my_frog) < 20:
            game_on = False
            level_counter.game_over()
            break

screen.exitonclick()
