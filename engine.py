from turtle import Turtle
import random


class Cars:
    def __init__(self):
        # cars color variations
        self.car_colors = ['red', 'yellow', 'blue', 'green', 'brown', 'orange', 'purple']
        self.all_cars = []
        # number of pre-created cars
        #self.create_car(50)

    def create_car(self, amount):
        for ii in range(amount):
            c_color = random.choice(self.car_colors)
            car = Turtle("square")
            car.color(c_color)
            car.pu()
            car.speed("fastest")
            car.shapesize(stretch_len=2, stretch_wid=1)
            s_x = random.randrange(240, 500, 40)
            s_y = random.randrange(-240, 250, 20)
            car.setx(s_x)
            car.sety(s_y)
            self.all_cars.append(car)

    def move_car(self):
        for car in range(len(self.all_cars)):
            new_x = self.all_cars[car].xcor() - 20
            self.all_cars[car].goto(new_x, self.all_cars[car].ycor())


class Frog(Turtle):
    def __init__(self):
        super().__init__()
        self.shape("turtle")
        self.color("black")
        self.pu()
        self.left(90)
        self.speed("fastest")

    def create_frog(self):
        self.setx(0)
        self.sety(-280)

    def move_fwd(self):
        self.goto(self.xcor(), self.ycor() + 20)

    def reset_frog(self):
        self.goto((0, -280))


class Level(Turtle):
    def __init__(self):
        super().__init__()
        self.s_level = 0
        self.pu()
        self.goto(-260, 260)
        self.color("black")
        self.ht()
        self.update_scoreboard()

    def update_scoreboard(self):
        self.clear()
        self.write(f"Level: {self.s_level}", font=("Arial", 18, "normal"))

    def increase_level(self):
        self.s_level += 1
        self.update_scoreboard()

    def game_over(self):
        self.goto(-80, 0)
        self.write("G A M E   O V E R", font=("Arial", 24, "bold"))
